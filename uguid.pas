unit uguid;

interface

function CreateGUID(out Guid: TGUID): HResult;
function GUIDToString(const GUID: TGUID): string;

implementation

function CoCreateGuid(out guid: TGUID): HResult; stdcall; external 'ole32.dll' name 'CoCreateGuid';

function StringFromCLSID(const clsid: TGUID; out psz: PWideChar): HResult; stdcall;
  external 'ole32.dll' name 'StringFromCLSID';

procedure CoTaskMemFree(pv: Pointer); stdcall;
  external 'ole32.dll' name 'CoTaskMemFree';


function Succeeded(Status: HRESULT): LongBool;
begin
  Result := Status and HRESULT($80000000) = 0;
end;

function CreateGUID(out Guid: TGUID): HResult;
begin
  Result := CoCreateGuid(Guid);
end;

function GUIDToString(const GUID: TGUID): string;
var
  P: PWideChar;
begin
  if not Succeeded(StringFromCLSID(GUID, P)) then begin
    Result := '';
    Exit;
  end;
  Result := P;
  CoTaskMemFree(P);
end;


end.
