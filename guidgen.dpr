program guidgen;

uses
  kol,
  uconst in 'uconst.pas',
  uguid in 'uguid.pas';

function fCreateForm(App : PControl): PControl;
begin
  Result := NewForm(App, ctFullName);
  Result.SetSize(320,60).CenterOnParent;
end;

function fGenerateGUID(eControl: PControl): HRESULT;
var g : TGUID;
begin
  Result := CreateGuid(g);
  eControl.Text := LowerCase(Copy( GUIDToString(g), 2, Length(GUIDToString(g)) - 2 ));
  eControl.SelectAll;
end;

procedure edDblClick(Dummy: Pointer; Sender : PControl; var Mouse : TMouseEventData);
begin
  fGenerateGUID(Sender);
end;

function fCreateGUIDField(Form : PControl): PControl;
begin
  Result := NewEditbox(Form, [eoNoHScroll, eoNoVScroll, eoReadonly]);
  Result.Align := caClient;
  Result.OnMouseDblClk := TOnMouse( MakeMethod( nil, @edDblClick));
end;


var
  fmM : PControl;
  edG : PControl;
begin
  Applet := NewApplet(ctAppName).CenterOnParent;
  Applet.Visible := True;
  fmM := fCreateForm(Applet);
  edG := fCreateGUIDField(fmM);
  fGenerateGUID(edG);
  Run(fmM);
end.
